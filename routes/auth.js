const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = mongoose.model("User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_KEY } = require("../keys");
const protection = require("../middlewares/RequireLogin");

router.get("/", (re, res) => {
  res.json({ message: " hello" });
});

router.get("/protect", protection, (req, res) => {
  res.send("Hello user!!!!");
});

router.post("/signup", (req, res) => {
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    return res.status(422).json({ error: "Please Enter All Fields!!!" });
  }
  //res.json({message:"You are successfully registred!"})
  User.findOne({ email: email })
    .then((saveduser) => {
      if (saveduser) {
        return res.json({ error: "User with this email exist." });
      }
      bcrypt.hash(password, 12).then((hashedPassword) => {
        if (hashedPassword) {
          const user = new User({
            email,
            name,
            password: hashedPassword,
          });
          user
            .save()
            .then((user) => {
              res.json({ message: "user registered successfully." });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      });
    })
    .catch((err) => {
      console.log(err);
    });
});

router.post("/signin", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.json({ error: "please enter email or password" });
  }
  User.findOne({ email: email })
    .then((user) => {
      if (user) {
        bcrypt
          .compare(password, user.password)
          .then((isMatch) => {
            if (isMatch) {
              const token = jwt.sign({ _id: user._id }, JWT_KEY);
              const { _id, name, email } = user;
              res.json({
                token,
                user: { _id, name, email },
                message: "You are signed in successfully",
              });
            }
            return res.json({
              error: "Please enter correct email or password",
            });
          })
          .catch((err) => console.log(err));
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

module.exports = router;
