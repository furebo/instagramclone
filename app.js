const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
//const authroute = require('./routes/auth');
const { MONGODB_URI } = require("./keys");

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on("connected", () => {
  console.log("Connected to Mongodb database!!");
});
mongoose.connection.on("error", (error) => {
  console.log("error connecting to Mongodb :", error);
});

require("./models/User");
require("./models/Post");

const PORT = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());
app.use(require("./routes/auth"));
app.use(require("./routes/post"));
app.use(require("./routes/user"));

app.listen(PORT, () => {
  console.log("Server is listening on port " + PORT);
});
